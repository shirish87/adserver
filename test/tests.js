
var env = (typeof process.env.NODE_ENV !== "undefined") ? process.env.NODE_ENV : "test";
if (env !== "test" && (typeof process.env.FORCE === "undefined" || process.env.FORCE !== "1")) {
    throw new Error("Invalid env");
}

console.log("env:", env);

process.env.NODE_ENV = env;


var Helpers = require("./inc/helpers")
    , should = Helpers.should
    , response = Helpers.response
    , DbUtils = require("./inc/dbutils")
    , ConfigModule = require("../lib/config")
    , conf
    , request
    , API_ROOT


before(function (done) {
    var conf = new ConfigModule
        .ConfigLoader()
        .forceEnv(env, true)
        .load()
        .getConfig();
    var dbUri = conf.db.dbUri;
    console.log("Clearing database:", dbUri);

    DbUtils.connect(dbUri, function () {
        DbUtils.clearDB(function (err) {
            if (err) {
                return done(err);
            }
            DbUtils.disconnect();
            return done();
        });
    });
});


before(function (done) {
    this.timeout(5000);

    var Config = ConfigModule.forceEnv(env, true);
    var appLoader = require("../app").appLoader;

    appLoader.on(appLoader.coreEvents.done, function () {
        var app = appLoader.app;

        // Set vars
        conf = Config.getConfig();

        API_ROOT = conf.api.v1.path;
        request = new Helpers.UserAgent(app).request;

        should.exist(conf);
        conf.env.should.equal(env);

        return done();
    });
});


describe("HTTP tests", function () {
    var state = {};

    it("API-server is available", function (done) {
        request()
            .get(API_ROOT + "/")
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.version.should.equal(1);
                done();
            }));
    });


    it("Create user", function (done) {
        var data = {
            "username": "shirish"
          , "fname": "Shirish"
          , "lname": "Kamath"
        };

        request()
            .post([ API_ROOT, "users" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");

                Object.keys(data).forEach(function (k) {
                    res.response.user[k].should.equal(data[k]);
                });

                state.user = res.response.user;
                done();
            }));
    });


    it("Create ad", function (done) {
        var data = {
            "title": "Hello World"
          , "path": "/content/1/1.mp4"
          , "thumbPath": "/content/1/thumbs"
          , "thumbCount": 4
          , "duration": 600
        };

        request()
            .post([ API_ROOT, state.user.username, "ads" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");

                Object.keys(data).forEach(function (k) {
                    res.response.ad[k].should.equal(data[k]);
                });

                should.equal(res.response.ad.active, 1);
                state.ad = res.response.ad;
                done();
            }));
    });


    it("Get ad by id", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads", state.ad._id ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ad._id.should.equal(state.ad._id);
                done();
            }));
    });


    it("Delete ad by id", function (done) {
        request()
            .del([ API_ROOT, state.user.username, "ads", state.ad._id ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.success.should.equal(true);
                done();
            }));
    });


    it("Create new ad", function (done) {
        var data = {
            "title": "Hello World 2"
          , "path": "/content/2/2.mp4"
          , "thumbPath": "/content/2/thumbs"
          , "thumbCount": 4
          , "duration": 600
        };

        request()
            .post([ API_ROOT, state.user.username, "ads" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");

                Object.keys(data).forEach(function (k) {
                    res.response.ad[k].should.equal(data[k]);
                });

                state.ad = res.response.ad;
                done();
            }));
    });


    it("List ads since date", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads" ].join("/"))
            .query({ "since": state.ad.created })
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ads.should.have.lengthOf(1);
                res.response.ads[0]._id.should.equal(state.ad._id);
                done();
            }));
    });


    it("List ads for user", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads" ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ads.should.have.lengthOf(1);
                res.response.ads[0]._id.should.equal(state.ad._id);
                done();
            }));
    });


    it("Add stats to ad", function (done) {
        var stat = {
            "duration": 12
          , "created": new Date().toISOString()
        };

        var data = {
            "stats": [ stat ]
        };

        request()
            .post([ API_ROOT, state.user.username, "ads", state.ad._id, "stats" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ad.should.be.an("object");

                Object.keys(stat).forEach(function (k) {
                    res.response.stat[k].should.equal(stat[k]);
                });

                state.ad = res.response.ad;
                done();
            }));
    });

    it("Bulk-add stats to ads", function (done) {
        var stat = {
            "duration": 18
          , "created": new Date().toISOString()
        };

        var data = {};
        data[state.ad._id] = {
            "stats": [ stat ]
        };

        request()
            .post([ API_ROOT, state.user.username, "ads", "stats" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.results.should.be.an("object");
                res.response.results[state.ad._id].stat.created.should.equal(stat.created);
                done();
            }));
    });


    it("Get ad stats by ad id", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads", state.ad._id, "stats" ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.stats.should.be.an("array");
                res.response.stats.should.have.lengthOf(2);
                done();
            }));
    });


    it("Set ad inactive", function (done) {
        var data = {
            "active": 0
        };

        request()
            .post([ API_ROOT, state.user.username, "ads", state.ad._id, "active" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ad.should.be.an("object");
                should.equal(res.response.ad.active, 0);
                state.ad = res.response.ad;
                done();
            }));
    });


    it("List ads for user should not contain inactive ads", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads" ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ads.should.have.lengthOf(0);
                done();
            }));
    });

    it("Set ad active", function (done) {
        var data = {
            "active": 1
        };

        request()
            .post([ API_ROOT, state.user.username, "ads", state.ad._id, "active" ].join("/"))
            .send(data)
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ad.should.be.an("object");
                should.equal(res.response.ad.active, 1);
                state.ad = res.response.ad;
                done();
            }));
    });


    it("List ads for user should contain active ads", function (done) {
        request()
            .get([ API_ROOT, state.user.username, "ads" ].join("/"))
            .set("Accept", "application/json")
            .end(response(done, function(res) {
                res.should.be.an("object");
                res.response.should.be.an("object");
                res.response.ads.should.have.lengthOf(1);
                done();
            }));
    });
});
