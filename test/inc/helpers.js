/**
 * Created by shirish on 10/11/13.
 */

var request = require('supertest')
  , chai = require('chai')
  , util = require('util')


chai.Assertion.includeStack = true;


module.exports.should = chai.should();


module.exports.response = function testHandler(done, next) {
    return (function (err, res) {
        var headers = !!res ? res.headers : undefined;
        var body = !!res ? res.body : undefined;
        if (err) {
            console.log(err, body);
            return done(err, body);
        }
        return next(body, headers);
    });
}


var UserAgent = function (a) {

    var app = a;

    this.request = function (options) {
        return request(app);
    }

    this.agent = function () {
        return request.agent(app);
    }
};


module.exports.UserAgent = UserAgent;


global.print = function (name, obj) {
    if (arguments.length == 1) {
        obj = name;
        name = '';
    }
    console.log(name, util.inspect(obj, { depth: null }));
}