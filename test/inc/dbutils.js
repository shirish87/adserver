/**
 * Created by shirish on 9/11/13.
 */

var mongoose = require('mongoose');


var clearDB = module.exports.clearDB = function (done) {
    mongoose.connection.db.collections(function (err, collections) {
        if (err) {
            throw err;
        }

        collections = collections.filter(function (collection) {
           return !collection.collectionName.match(/system/);
        });

        var totalOps = collections.length;
        var i = 0;

        if (totalOps === 0) {
            return done();
        }

        collections.forEach(function (collection) {
            collection.drop(function (e) {
                if (e) console.log(e);
                if (++i === totalOps) {
                    done && done();
                }
            });
        });
    });
}

module.exports.findOne = function (collec, query, callback) {
    mongoose.connection.db.collection(collec, function (err, collection) {
        if (err) {
            return callback(err);
        }
        collection.findOne(query, callback);
    });
};

module.exports.find = function (collec, query, callback) {
    mongoose.connection.db.collection(collec, function (err, collection) {
        if (err) {
            return callback(err);
        }
        collection.find(query, callback);
    });
};


var self = this;

module.exports.init = function (dbUri) {
    console.log('dbUri', dbUri);

    before(function (done) {
        checkState(dbUri, done);
    });

    after(function (done) {
        mongoose.disconnect();
        return done();
    });

    return self;
}

var disconnect = module.exports.disconnect = function () {
    mongoose.disconnect();
}


var connect = module.exports.connect = function (dbUri, done) {
    mongoose.connect(dbUri, function (err) {
        if (err) {
            throw err;
        }
        return done();
    });
}


var checkState = module.exports.checkState = function (dbUri, done) {
    switch (mongoose.connection.readyState) {
        case 0:
            connect(dbUri, done);
            break;
        case 1:
            done();
            break;
        default:
            setImmediate(function () {
                checkState(dbUri, done);
            });
    }
}