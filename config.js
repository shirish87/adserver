
module.exports.env = process.env.NODE_ENV || 'development';


module.exports.app = {};

module.exports.app.defaults = {
    name: 'project_z',
    env: {
        host: process.env.HOST || '127.0.0.1',
        port: process.env.PORT || 3000,
        protocol: process.env.PROTO || 'http',
        rootDir: __dirname
    },
    session: {
        key: '_sess',
        secret: 'sUp3rS3cUr3S3cr3t',
        cookie: {
            maxAge: 30 * 24 * 60 * 60 * 1000
        }
    }
};


module.exports.api = {};

module.exports.api.defaults = {
    v1: {
        path: '/api/v1'
    }
};


module.exports.db = {};

module.exports.db.defaults = {
    dbUri: process.env.MONGODB_URI
};

module.exports.db.development = {
    dbUri: 'mongodb://adserver:mUchach0@linus.mongohq.com:10022/MessagesMaster'
};

