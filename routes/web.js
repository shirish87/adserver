
var ResourceController = require('../controller/resource').ResourceController
  , debug = require('debug')('app:routes:web')


module.exports.moduleName = 'web';

module.exports.priority = 0;


module.exports.public = function (app, config) {
    app.get('/login', function (req, res) {
        res.ext.view('login').render();
    });

    app.get('/', ResourceController.getPublic);
}


module.exports.private = function (app, config) {
    app.get('/resources/resource', ResourceController.getProtected);
}
