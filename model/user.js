

var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , _ = require('lodash')
  , Helper = require('./helper')
  , Errors = require('./error').Errors


var userSchema = new Schema({
    username:       { type: String, required: true, index: { unique: true } },
    fname:          { type: String, required: true },
    lname:          { type: String },
    created:        { type: Date, required: true, default: Date.now },
    modified:       { type: Date, required: true, default: Date.now }
});

var USER_ATTRS = _.keys(userSchema.paths);


userSchema.statics.create = function (attrs, callback) {
    var UserModel = this, newUser = new UserModel();
    Helper.copyAttrs(USER_ATTRS, attrs, newUser);

    newUser.created = Date.now();
    newUser.save(function (err) {
        if (err) {
            return callback(Errors.DB_FAIL.debug(err));
        }

        return callback(null, newUser);
    });
}


module.exports.UserModel = mongoose.model('user', userSchema, 'user');