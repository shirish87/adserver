
var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , Helper = require('./helper')
  , Errors = require('./error').Errors
  , _ = require('lodash')


var StatsSchema = new Schema({
    duration:       { type: Number, required: true, default: 0 },
    created:        { type: Date, required: true, default: Date.now },
    ip:             { type: String },
    meta:           { type: Schema.Types.Mixed }
});


var ViewsSchema = new Schema({
    created:        { type: Date, required: true },
    meta:           { type: Schema.Types.Mixed }
});


var AdSchema = new Schema({
    username:       { type: String, required: true },
    created:        { type: Date, required: true, default: Date.now },
    modified:       { type: Date, required: true, default: Date.now },
    active:         { type: Number, required: true, default: 1, enum: [ 0, 1 ] },

    path:           { type: String, required: true },
    title:          { type: String, required: true },
    duration:       { type: Number, required: true, default: 0 },

    thumbPath:      { type: String, required: true },
    thumbCount:     { type: Number, required: true, default: 0 },

    viewCount:      { type: Number, required: true, default: 0 },
    playCount:      { type: Number, required: true, default: 0 },

    views:          [ ViewsSchema ],
    stats:          [ StatsSchema ]
});


var AD_ATTRS = _.omit(_.keys(AdSchema.paths), [ 'stats', 'views', 'active' ]);
var STAT_ATTRS = _.keys(StatsSchema.paths);
var VIEW_ATTRS = _.keys(ViewsSchema.paths);


AdSchema.methods.toJSON = function () {
    return _.omit(this.toObject(), [ '__v', 'status', 'views' ]);
}


AdSchema.statics.create = function (attrs, callback) {
    var AdModel = this, newAd = new AdModel();
    Helper.copyAttrs(AD_ATTRS, attrs, newAd);

    newAd.save(function (err) {
        if (err) {
            return callback(Errors.DB_FAIL.debug(err), null);
        }

        return callback(null, newAd);
    });
};


AdSchema.statics.del = function (id, username, callback) {
    this.remove({ _id: id, username: username }, function (err) {
        if (err) {
            return callback(Errors.AD_NOT_FOUND.debug(err));
        }

        return callback();
    });
};


AdSchema.statics.addStats = function (attrs, callback) {
    var stats = _.map(attrs.stats, function (stat) {
        return _.pick(stat, STAT_ATTRS);
    });

    var views = _.map(attrs.views, function (view) {
        return _.pick(view, VIEW_ATTRS);
    });

    var updates = {
        $push: {},
        $inc: {}
    };

    if (Array.isArray(stats) && stats.length > 0) {
        updates.$push.stats = {
            $each: stats
        };

        updates.$inc.playCount = stats.length;
    }

    if (Array.isArray(views) && views.length > 0) {
        updates.$push.views = {
            $each: views
        };

        updates.$inc.viewCount = views.length;
    }

    if (!updates.$push.stats && !updates.$push.views) {
        return callback(Errors.INVALID_ARGUMENTS);
    }

    return this.updateStats(attrs._id, updates, callback);
};


AdSchema.statics.updateStats = function (_id, updates, callback) {
    this.findByIdAndUpdate(_id, updates, function (err, doc) {
        if (err) {
            return callback(Errors.DB_FAIL.debug(err));
        }

        return callback(null
          , doc
          , doc.stats.slice(-1)[0]
          , doc.views.slice(-1)[0]);
    });
}


AdSchema.statics.setActive = function (_id, username, active, callback) {
    this.findOneAndUpdate({
        _id: _id
      , username: username
    }, {
        $set: {
            active: active
        }
    }, function (err, doc) {
        if (err) {
            return callback(Errors.DB_FAIL.debug(err));
        }

        return callback(null, doc);
    });
}


module.exports.AdModel = mongoose.model('ad', AdSchema, 'ad');
