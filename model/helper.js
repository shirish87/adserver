
var _ = require('lodash')


module.exports.copyAttrs = function(attrs, src, dest) {
    _.each(attrs || {}, function (k) {
        if (typeof src[k] === 'undefined') {
            return;
        }

        if (typeof src[k] !== 'object' || Array.isArray(src[k]) || typeof dest[k] === 'undefined') {
            dest.set(k, src[k]);
        } else {
            _.merge(dest[k], src[k]);
        }
    });
    return dest;
}