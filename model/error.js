

var errors = {}, errorMsgs = {};

errorMsgs.DB_FAIL = 'Storage error. Please try again later.';
errorMsgs.BAD_REQUEST = 'Bad request';
errorMsgs.AD_NOT_FOUND = 'Invalid ad';
errorMsgs.INVALID_AD = 'Invalid ad';
errorMsgs.USER_NOT_FOUND = 'Invalid user';
errorMsgs.INVALID_ARGUMENTS = 'Invalid arguments';

Object.keys(errorMsgs).forEach(function (err) {
    var e = new Error(err);
    e.id = err;
    e.m = errorMsgs[err];
    e.d = null;
    e.e = null;
    e.error = function (e) {
        this.e = e;
        return this;
    }
    e.debug = function (d) {
        this.d = d;
        return this;
    };
    e.toString = function () {
        return e.d || e.m;
    };

    errors[err] = e;
});


module.exports = {
    Errors: errors
}
