
"Sync" Ads Algorithm
--------------------

 1. Fetch list of ads for current user from the server into <ad-list> (GET /:username/ads)
 2. Periodically (or explicitly on receiving PUSH) sync local ads list with remote ads
     1. <transact-start>
     2. Mark all local entries for ads as "inactive"
     3. For each <ad> in <ad-list>
         1. CreateOrUpdate(ad)
         2. Update: Mark <ad> as active
     4. <transact-end>
 3. Periodically delete all local ads marked inactive

Send Stats
----------

 1. Store events locally in SQLite
 2. Periodically assemble stored stats, build stats JSON object and POST /:username/ads/stats
