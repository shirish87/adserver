

function ResourceController() {

}


ResourceController.prototype.getPublic = function (req, res) {
    res.ext.data({
        answer: 41
      , version: 1
    }).view('index').render();
}


ResourceController.prototype.getProtected = function (req, res) {
    res.ext.data({
        answer: 42
      , version: 1
    }).view('index').render();
}


module.exports.ResourceController = new ResourceController();
