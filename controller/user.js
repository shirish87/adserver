
var Validator = require('validatorjs')
  , UserModel = require('../model/user').UserModel
  , _ = require('lodash')
  , debug = require('debug')('app:controller:user')


function UserController() {

}


UserController.prototype.create = function (req, res) {
    var attrs = req.body;

    if (!attrs || typeof attrs !== 'object') {
        return res.ext.error('Invalid request')
            .code(res.ext.STATUS.BAD_REQUEST)
            .render();
    }

    var validation = new Validator(attrs, {
        username: 'username',
        fname: 'required|max:120',
        lname: 'required|max:120'
    });

    if (validation.fails()) {
        return res.ext.error(_.map(_.keys(validation.errors), function (errField) {
            return validation.errors.first(errField);
        })).render();
    }


    UserModel.create(attrs, function (err, doc) {
        if (err) {
            return res.ext.error(err).render();
        }

        return res.ext.data({ user: doc.toJSON() }).render();
    });
}



module.exports.UserController = new UserController();
