
var Validator = require('validatorjs')
  , AdModel = require('../model/ad').AdModel
  , Errors = require('../model/error').Errors
  , async = require('async')
  , _ = require('lodash')
  , debug = require('debug')('app:controller:ad')





function getErrors(validation) {
    return _.map(validation.errors
      , function (fieldErrs, errField, vldErrors) {
            return vldErrors.first(errField);
        });
}


function AdController() {

}


AdController.prototype.create = function (req, res) {
    var attrs = req.body;

    if (!attrs || typeof attrs !== 'object') {
        return res.ext.error('Invalid request')
                .code(res.ext.STATUS.BAD_REQUEST)
                .render();
    }

    attrs.username = req.extras.user.username;

    var validation = new Validator(attrs, {
        username: 'username'
      , title: 'required|max:60'
      , path: 'required'
      , duration: 'required|integer|min:1|max:10000'
      , thumbPath: 'required'
      , thumbCount: 'required|integer|min:1'
    });

    if (validation.fails()) {
        return res.ext.error().render();
    }

    AdModel.create(attrs, function (err, doc) {
        if (err) {
            return res.ext.error(err).render();
        }

        return res.ext.data({ ad: doc.toJSON() }).render();
    });
}


AdController.prototype.get = function (req, res) {
    var adId = req.param('ad');

    if (!adId) {
        return res.ext.error('Invalid ad')
                .code(res.ext.STATUS.BAD_REQUEST)
                .render();
    }

    AdModel.findOne({
        _id: adId
      , username: req.extras.user.username
      , active: 1
    }).select('-v -stats').exec(function (err, ad) {
        if (err) {
            return res.ext.error(err).render();
        }

        if (!ad) {
            return res.ext.error(Errors.AD_NOT_FOUND)
                    .code(res.ext.STATUS.NOT_FOUND)
                    .render();
        }

        return res.ext.data({ ad: ad || {} }).render();
    });
}


AdController.prototype.del = function (req, res) {
    var adId = req.param('ad');

    if (!adId) {
        return res.ext.error('Invalid ad')
                .code(res.ext.STATUS.BAD_REQUEST)
                .render();
    }

    AdModel.del(adId, req.extras.user.username, function (err) {
        if (err) {
            return res.ext.data({ success: false }).error(err).render();
        }

        return res.ext.data({ success: true }).render();
    });
}


AdController.prototype.setActive = function (req, res) {
    var adId = req.param('ad');
    var attrs = req.body;

    if (!adId || (attrs.active !== 0 && attrs.active !== 1)) {
        return res.ext.error('Invalid request')
                .code(res.ext.STATUS.BAD_REQUEST)
                .render();
    }

    AdModel.setActive(adId, req.extras.user.username, attrs.active, function (err, doc) {
        if (err) {
            return res.ext.data({ success: false }).error(err).render();
        }

        return res.ext.data({ success: true, ad: doc.toJSON() }).render();
    });
}


AdController.prototype.list = function (req, res) {
    var query = {
        username: req.extras.user.username
      , active: 1
    };

    if (req.query.since) {
        var validation = new Validator(req.query, {
            since: 'date'
        });

        if (validation.fails()) {
            return res.ext.error(getErrors(validation)).render();
        }

        query.created = { $gte: req.query.since };
    }

    AdModel.find(query).select('-v -stats').exec(function (err, ads) {
        if (err) {
            return res.ext.error(err).render();
        }

        return res.ext.data({ ads: ads || [] }).render();
    });
}


AdController.prototype.addStats = function (req, res) {
    var attrs = req.body;
    var adId = req.param('ad');

    addStats(adId, attrs, function (err, ad, stat, view) {
        if (err) {
            return res.ext.error(err).render();
        }

        ad = ad.toJSON();
        res.ext.data({ ad: ad });

        if (stat) {
            res.ext.data({ stat: stat });
        } else if (view) {
            res.ext.data({ view: view });
        }

        return res.ext.render();
    });
}


AdController.prototype.addStatsBulk = function (req, res) {
    var attrs = req.body;

    if (Array.isArray(attrs) || typeof attrs !== 'object' || !Object.keys(attrs).length) {
        return res.ext.error(Errors.BAD_REQUEST).render();
    }

    var tasks = {}, maxParallel = 5;

    _.each(attrs, function (adAttrs, adId) {
        tasks[adId] = function (callback) {
            addStats(adId, adAttrs, function (err, ad, stat, view) {
                return callback(err, {
                    ad: ad
                  , stat: stat
                  , view: view
                });
            });
        }
    });

    async.parallelLimit(tasks, maxParallel, function (err, results) {
        if (err) {
            return res.ext.error(err).render();
        }

        return res.ext.data({ results: results }).render();
    });
}


AdController.prototype.getStats = function (req, res) {
    var adId = req.param('ad');

    if (!adId) {
        return res.ext.error('Invalid ad')
                .code(res.ext.STATUS.BAD_REQUEST)
                .render();
    }

    AdModel.findOne({
        _id: adId
      , username: req.extras.user.username
      , active: 1
    }).select('stats').exec(function (err, ad) {
        if (err) {
            return res.ext.error(err).render();
        }

        if (!ad) {
            return res.ext.error(Errors.AD_NOT_FOUND)
                    .code(res.ext.STATUS.NOT_FOUND)
                    .render();
        }

        return res.ext.data({ stats: ad.stats || [] }).render();
    });
}



function addStats(adId, attrs, callback) {
    if (!attrs || !adId) {
        return callback(Errors.INVALID_AD);
    }

    var validation, updates = { _id: adId };
    var fields = [ 'stats', 'views' ];
    var validators = {
        stats: {
            duration: 'required|integer'
          , created: 'date'
        }
      , views: {
            created: 'date'
        }
    };

    for (var i = 0, l = fields.length; i < l; i +=1) {
        var k = fields[i];
        updates[k] = [];

        if (Array.isArray(attrs[k])) {
            updates[k] = attrs[k];
        } else if (typeof attrs[k] === 'object') {
            updates[k] = [ attrs[k] ];
        }

        for (var j = 0, m = updates[k].length; j < m; j += 1) {
            validation = new Validator(updates[k][j] || {}, validators[k]);

            if (validation.fails()) {
                return callback(getErrors(validation));
            }
        }
    }

    if (!updates.stats.length && !updates.views.length) {
        return callback(Errors.BAD_REQUEST);
    }

    AdModel.addStats(updates, function (err, ad, stat, view) {
        if (err) {
            return callback(err);
        }

        if (!stat && !view) {
            return callback(Errors.AD_NOT_FOUND);
        }

        return callback(null, ad, stat, view);
    });
}


module.exports.AdController = new AdController();