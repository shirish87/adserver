
var UserModel = require('../model/user').UserModel
  , Errors = require('../model/error').Errors
  , debug = require('debug')('app:app_modules:params')


var moduleName = 'params';

var priority = 2;


var load = module.exports.load = function (app, config) {
    app.param('username', function (req, res, next, username) {
        UserModel.findOne({ username: username }, function (err, user) {
            if (err || !user) {
                return res.ext.error(Errors.USER_NOT_FOUND.debug(err)).render();
            }

            req.extras = req.extras || {};
            req.extras.user = user;

            next();
        });
    });
}


module.exports.init = function (appLoader) {
    appLoader.hook(appLoader.events.router, {
        priority: priority
      , name: moduleName
    }, load);
}
