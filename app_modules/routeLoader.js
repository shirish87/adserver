
var RouteLoader = require('../lib/routeLoader').RouteLoader
  , debug = require('debug')('app:app_modules:routeLoader')


var moduleName = 'routeLoader';

var priority = 4;



var load = module.exports.load = function (app, config) {
    var routeLoader = new RouteLoader(app, config, {
        loginPath: '/login'
    });

    routeLoader.boot();
    routeLoader.debug();
}



module.exports.init = function (appLoader) {
    appLoader.hook(appLoader.events.router, {
        priority: priority
      , name: moduleName
    }, load);
}