

var mongoose = require('mongoose')
  , debug = require('debug')('app:mongoose')


var moduleName = module.exports.name = 'mongoose';

var priority = 0;



var load = module.exports.load = function (app, config, appLoader, waitKey, callback) {
    var dbUri = config.db.dbUri;

    mongoose.connect(dbUri, function (err) {
        if (err) {
            debug(err);
            console.log('Error connecting to %s', dbUri.split('/').slice(-1));
            return callback(err, waitKey);
        }

        console.log('Connected to %s', dbUri.split('/').slice(-1));
        appLoader.dispatchEvent(appLoader.events.db);
        return callback(null, waitKey);
    });
}


module.exports.init = function (appLoader) {
    var waitKey = appLoader.hook(appLoader.events.config, {
        priority: priority
      , name: moduleName
      , wait: true
    }, function (app, config, callback) {
        load(app, config, appLoader, waitKey, callback);
    });
}
