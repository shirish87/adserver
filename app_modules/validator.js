

var Validator = require('validatorjs')
    , _  = require('lodash')
    , ObjectId = require('mongoose').Types.ObjectId


var moduleName = module.exports.name = 'validator';

var priority = 0;


var REGEX_USERNAME = /^[a-z]+[a-z0-9_\-]{3,25}$/;
var REGEX_PASSWORD = /^[a-z0-9_\-\$\*\\@]{5,30}$/i;

function parseDate(str) {
    if (str) {
        var t = str.match(/^(\d{4})\-(\d{2})\-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})Z$/);
        if(t !== null){
            var d = +t[3], m = +t[2], y = +t[1];
            var date = new Date(y, m-1, d);
            if(date.getFullYear() === y && date.getMonth() === m - 1){
                return date;
            }
        }
    }

    return null;
}


var load = module.exports.load = function (app, config) {

    Validator.register('object_id', function (val) {
        try {
            var o = new ObjectId(val);
        } catch (e) {
            return false;
        }

        return true;
    }, 'Invalid :attribute');

    Validator.register('username', function (val) {
        return val && val.match(REGEX_USERNAME);
    }, 'Invalid :attribute');


    Validator.register('password', function (val) {
        return val && val.match(REGEX_PASSWORD);
    }, 'Invalid :attribute');

    Validator.register('date', function (val) {
        return (parseDate(val) !== null);
    }, 'Invalid :attribute');
}



module.exports.init = function (appLoader) {
    appLoader.hook(appLoader.events.config, {
        priority: priority
      , name: moduleName
    }, load);
}
