

var express = require('express')
    , namespace = require('express-namespace')
    , EventEmitter = require('./eventEmitter').EventEmitter
    , utils = require('./utils')
    , util = require('util')
    , path = require('path')
    , _ = require('lodash')


var appDefaults = {
    modules: [],
    exitOnModuleFail: true,
    waitTimeout: 5000,
    eventList: [ 'init', 'config', 'db', 'session', 'static', 'router', 'setup', 'start', 'ready', 'moduleError' ],
    isSubapp: false
};


var AppLoader = module.exports.AppLoader = function AppLoader(env, config, options) {
    AppLoader.super_.call(this);

    options = _.defaults(options || {}, appDefaults);

    var self = this;
    this.env = env;
    this.config = _.cloneDeep(config);

    if (options.rootDir) {
        this.rootDir = this.config.app.env.rootDir = options.rootDir;
    } else {
        this.rootDir = this.config.app.env.rootDir
    }

    this.modules = options.modules;
    this.exitOnModuleFail = options.exitOnModuleFail;

    this.eventList = options.eventList;
    this.events = {};

    this.eventList.forEach(function (v) {
        self.events[v] = v;
    });

    this.waitTimeout = options.waitTimeout;
    this.app = express();
    this.isSubapp = options.isSubapp;
    this.waitForModules = options.waitForModules || true;
};

util.inherits(AppLoader, EventEmitter);


AppLoader.prototype.init = function () {
    this.dispatchEvent(this.events.init);
    this.dispatchEvent(this.events.config);
    if (this.isSubapp) {
        this.dispatchEvent(this.events.db);
    }
}


AppLoader.prototype.start = function () {
    var host = this.config.app.env.host;
    var port = this.config.app.env.port;

    var self = this;
    this.app.listen(port, function () {
        console.log('Express server listening on port ' + port);
        self.dispatchEvent(self.events.start);
    });
}


AppLoader.prototype.setup = function () {
    var app = this.app;

    if (this.env === 'development') {
        app.use(express.logger('dev'));
    }

    app.set('views', path.join(this.rootDir, 'views'));
    app.set('view engine', 'jade');

    app.use(express.urlencoded());
    app.use(express.json());

    app.use(express.methodOverride());

    this.dispatchEvent(this.events.session);

    app.use(express.static(path.join(this.rootDir, 'public')));

    if (!this.isSubapp) {
        app.use(express.favicon(path.join(this.rootDir, 'public', 'favicon.ico')));
    }

    this.dispatchEvent(this.events.router);
    app.use(app.router);

    this.dispatchEvent(this.events.setup);
}


AppLoader.prototype.dispatchEvent = function (event) {
    this.emitEvent(event, this.app, this.config || {});
}


AppLoader.prototype.registerModules = function () {
    if (!this.modules || !this.modules.length) {
        return;
    }

    var self = this;

    this.modules.forEach(function (mod) {
        var modulesPath = path.join(self.rootDir, mod.dir);
        utils.scanModules(modulesPath, mod.searchDepth, function (module) {
            if (typeof module.init === 'function') {
                module.init(self);
            }
        });
    });

    this.finalizeListeners();
}


AppLoader.prototype.boot = function (done) {
    var self = this;
    this.on(this.coreEvents.error, function (err, moduleName) {
        if (err instanceof Error) {
            console.log('ModuleError ' + moduleName, err.message);
        } else {
            console.log('ModuleError ' + moduleName, err);
        }

        if (self.exitOnModuleFail) {
            process.exit(-1);
        }
    });

    var isDone = false
      , isStarted = false;

    var checkReady = function () {
        if (isDone && isStarted) {
            self.dispatchEvent(self.events.ready);
        }
    }

    // modules have been loaded
    this.on(this.coreEvents.done, function () {
        isDone = true;
        checkReady();

        if (typeof done === 'function') {
            return done(null, self.app);
        }

        if (!self.isSubapp && self.waitForModules) {
            self.start();
        }
    });

    // server is listening
    this.on(this.events.start, function () {
        isStarted = true;
        checkReady();
    });

    this.registerModules();
    this.debug();
    this.init();
    this.setup();
    this.checkWaitComplete();

    if (!this.isSubapp && !this.waitForModules) {
        this.start();
    }   
}


module.exports.SubAppLoader = function SubAppLoader(env, config, options) {
    options = options || {};
    options.isSubapp = true;
    options.waitForModules = true;
    return new AppLoader(env, config, options);
}
