/**
 * Created by shirish on 4/11/13.
 */

var fs = require('fs')
    , path = require('path')
    , util = require('util')
    , debug = require('debug')('app:lib:util')


module.exports.dump = function(name, obj) {
    if (arguments.length == 1) {
        obj = name;
        name = '';
    }
    console.log(name, util.inspect(obj, { depth: null }));
}


var scanModules = module.exports.scanModules = function(modulePath, maxDepth, fn) {
    maxDepth--;

    var files = [];
    try {
        files = fs.readdirSync(modulePath);
    } catch (e) {
        console.log('[ERROR] Skipped reading ' + e.code, modulePath);
    }

    files.forEach(function(file) {
        var newPath = path.join(modulePath, file);
        var stat = fs.statSync(newPath);
        if (stat.isFile()) {
            if (/(.*)\.(js|coffee)/.test(file)) {
                fn(require(newPath), newPath);
            }
        } else if (stat.isDirectory() && maxDepth > 0) {
            scanModules(newPath, maxDepth, fn);
        }
    });
}


module.exports.loadPlugins = function loadPlugins(plugins, rootDir, requiresInit) {
    var pluginList = [];
    plugins.forEach(function (mod) {
        var modulesPath = path.join(rootDir, mod.dir);
        scanModules(modulesPath, mod.searchDepth, function (module, modulePath) {
            if (typeof module.priority !== 'number') {
                console.log('[ERROR] Skipped loading plugin ' + modulePath + ' (no priority)');
                return;
            }

            if (module.priority === -1) return;  // module explicitly requests skipping

            if (typeof module.moduleName !== 'string') {
                console.log('[ERROR] Skipped loading plugin ' + modulePath + ' (no moduleName)');
                return;
            }

            if (requiresInit && typeof module.init !== 'function') {
                console.log('[ERROR] Skipped loading plugin ' + modulePath + ' (no init)');
                return;
            }

            pluginList.push(module);
        });
    });

    return pluginList.sort(function (a, b) {
        return a.priority - b.priority;
    });
}


module.exports.moduleExists = function(module) {
    try {
        require.resolve(module);
        return true;
    } catch (ignored) {
    }
    return false;
}
