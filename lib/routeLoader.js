

var EventEmitter = require('../lib/eventEmitter').EventEmitter
  , methods = require('methods').concat('del')
  , login = require('connect-ensure-login')
  , util = require('util')
  , utils = require('../lib/utils')
  , path = require('path')
  , _ = require('lodash')
  , debug = require('debug')('app:lib:routeLoader')


var defaults = {
    maxDepth: 2,
    routeTypes: [ 'public', 'validate', 'protected', 'private' ],
    privateRoute: 'private',
    loginPath: '/login'
};


var RouteLoader = module.exports.RouteLoader = function RouteLoader(app, config, options) {
    RouteLoader.super_.call(this);

    options = _.defaults(options || {}, defaults);

    this.routeTypes = this.eventList = options.routeTypes;
    this.events = {};

    this.privateRoute = options.privateRoute;
    this.loginPath = options.loginPath;

    var self = this;
    self.eventList.forEach(function (v) {
        self.events[v] = v;
    });

    this.app = app;
    this.config = config;
}

util.inherits(RouteLoader, EventEmitter);


RouteLoader.prototype.emitEvent = function (event) {
    this.emit(event, this.app, this.config || {});
}


RouteLoader.prototype.registerModules = function () {
    var plugins = [{ dir: 'routes', searchDepth: 1 } ]

    var self = this;
    utils.loadPlugins(plugins, this.config.app.env.rootDir).forEach(function (module) {
        self.routeTypes.forEach(function (rType) {
            var fnRoute = module[rType];
            if (typeof fnRoute === 'function') {
                if (rType === self.privateRoute) {
                    self.hook(rType, {
                        priority: module.routeOrder,
                        name: module.moduleName
                    }, function (app, config) {
                        fnRoute.call(module, self.privateApp(app), config);
                    });
                } else {
                    self.hook(rType, {
                        priority: module.routeOrder,
                        name: module.moduleName
                    }, fnRoute.bind(module));
                }
            }
        });
    });

    this.finalizeListeners();
}


RouteLoader.prototype.start = function () {
    var self = this;
    this.routeTypes.forEach(function (rType) {
        self.emitEvent(rType);
    });
}


RouteLoader.prototype.privateApp = function (app) {
    var privateApp = {};
    privateApp.namespace = app.namespace.bind(app);

    var self = this;
    methods.forEach(function (method) {
        var orig = app[method];

        privateApp[method] = function (val) {
            if ('get' === method && 1 === arguments.length) {
                return orig.call(app, val);
            }

            var args = Array.prototype.slice.call(arguments);
            var path = args.shift();

            args.unshift(path, self.requireLogin());
            orig.apply(app, args);

            return this;
        };
    });

    return privateApp;
}


RouteLoader.prototype.boot = function () {
    this.registerModules();
    this.start();
}


RouteLoader.prototype.requireLogin = function () {
    return login.ensureLoggedIn(this.loginPath);
}