

var SubAppLoader = require('../../lib/appLoader').SubAppLoader
    , debug = require('debug')('app:modules:api-v1')


var moduleName = module.exports.name = 'modules:api-v1';

/* CONFIG */
var priority = 6;
var waitTimeout = 5000;
var CURR_DIR = __dirname;
var modules = [
    { dir: 'app_modules', searchDepth: 1 }
];
var exitOnModuleFail = true;


module.exports.init = function(appLoader) {
    var subappLoader = new SubAppLoader(appLoader.env, appLoader.config, {
        rootDir: CURR_DIR,
        modules: modules,
        waitTimeout: waitTimeout,
        exitOnModuleFail: exitOnModuleFail
    });

    appLoader.hook(appLoader.events.router, {
        priority: priority,
        name: moduleName
    }, function (app, config) {
        var mountPath = config.api.v1.path;

        subappLoader.boot(function (err, subapp) {
            app.use(mountPath, subapp);
            console.log('Loaded subapp ' + moduleName + ' at', mountPath);
        });
    });
}
