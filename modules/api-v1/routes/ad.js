
var AdController = require('../../../controller/ad').AdController
  , debug = require('debug')('app:modules:api-v1:routes:ad')


module.exports.moduleName = 'ad';

module.exports.priority = 1;


module.exports.public = function (app, config) {
    app.namespace('/:username/ads', function () {
        app.get('/:ad/stats', AdController.getStats);
        app.post('/:ad/stats', AdController.addStats);
        app.post('/:ad/active', AdController.setActive);
        app.get('/:ad', AdController.get);
        app.del('/:ad', AdController.del);
        app.post('/stats', AdController.addStatsBulk);
        app.get('/', AdController.list);
        app.post('/', AdController.create);
    });
}
