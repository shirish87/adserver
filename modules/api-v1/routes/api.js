
var ResourceController = require('../../../controller/resource').ResourceController
  , debug = require('debug')('app:modules:api-v1:routes:api')


module.exports.moduleName = 'api';

module.exports.priority = 0;


module.exports.public = function (app, config) {
    app.get('/', ResourceController.getPublic);
}


module.exports.validate = function (app, config) {
    app.all('/resources/*', function (req, res, next) {
        next();
    });
}


module.exports.protected = function (app, config) {
    app.get('/resources/resource', ResourceController.getProtected);
}
