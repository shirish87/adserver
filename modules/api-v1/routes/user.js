
var UserController = require('../../../controller/user').UserController
  , debug = require('debug')('app:modules:api-v1:routes:user')


module.exports.moduleName = 'user';

module.exports.priority = 1;


module.exports.public = function (app, config) {
    app.post('/users', UserController.create);
}
