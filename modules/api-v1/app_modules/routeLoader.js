
var RouteLoader = require('../../../app_modules/routeLoader')
    , debug = require('debug')('app:modules:api-v1:routeLoader')


var moduleName = module.exports.name = 'modules:routeLoader';


var load = module.exports.load = function(app, config) {
    RouteLoader.load(app, config);
}


module.exports.init = function(appLoader) {
    appLoader.hook(appLoader.events.router, {
        priority: 1
      , name: moduleName
    }, load);
}
