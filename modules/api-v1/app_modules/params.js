
var Params = require('../../../app_modules/params')
  , debug = require('debug')('app:modules:api-v1:params')


var moduleName = module.exports.moduleName = 'modules:params';

var priority = 0;


var load = module.exports.load = function(app, config) {
    Params.load(app, config);
}


module.exports.init = function(appLoader) {
    appLoader.hook(appLoader.events.router, {
        priority: priority
      , name: moduleName
    }, load);
}
