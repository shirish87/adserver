
var Config = require('./lib/config')
  , AppLoader = require('./lib/appLoader').AppLoader


/* CONFIG */
var modules = [
    { dir: 'app_modules', searchDepth: 1 },
    { dir: 'modules', searchDepth: 2 }
];


Config.load();

var appLoader = new AppLoader(Config.getEnv(), Config.getConfig(), {
    modules: modules,
    exitOnModuleFail: true,
    waitTimeout: 5000
});
appLoader.boot();


// For test-hooks
module.exports.appLoader = appLoader;
module.exports.app = appLoader.app;
